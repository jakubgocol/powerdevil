# Translation of kcm_powerdevilprofilesconfig.po to Catalan
# Copyright (C) 2010-2024 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# SPDX-FileCopyrightText: 2023 Antoni Bella Pérez <antonibella5@yahoo.com>
# Josep M. Ferrer <txemaq@gmail.com>, 2010, 2011, 2012, 2017, 2020, 2021, 2023, 2024.
msgid ""
msgstr ""
"Project-Id-Version: powerdevil\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-03-10 00:39+0000\n"
"PO-Revision-Date: 2024-01-12 19:14+0100\n"
"Last-Translator: Josep M. Ferrer <txemaq@gmail.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 22.12.3\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Accelerator-Marker: &\n"

#: PowerKCM.cpp:442
#, kde-format
msgid "The Power Management Service appears not to be running."
msgstr "Sembla que el servei de gestió d'energia no s'està executant."

#: ui/GlobalConfig.qml:16
#, kde-format
msgctxt "@title"
msgid "Advanced Power Settings"
msgstr "Arranjament avançat d'energia"

#: ui/GlobalConfig.qml:21
#, kde-format
msgctxt ""
"Percentage value example, used for formatting battery levels in the power "
"management settings"
msgid "10%"
msgstr "10%"

#: ui/GlobalConfig.qml:57
#, kde-format
msgctxt "@title:group"
msgid "Battery Levels"
msgstr "Nivells de la bateria"

#: ui/GlobalConfig.qml:65
#, kde-format
msgctxt ""
"@label:spinbox Low battery level percentage for the main power supply battery"
msgid "&Low level:"
msgstr "Nive&ll baix:"

#: ui/GlobalConfig.qml:73
#, kde-format
msgctxt "@accessible:name:spinbox"
msgid "Low battery level"
msgstr "Nivell baix de la bateria"

#: ui/GlobalConfig.qml:98
#, kde-format
msgctxt "@info:whatsthis"
msgid ""
"The battery charge will be considered low when it drops to this level. "
"Settings for low battery will be used instead of regular battery settings."
msgstr ""
"La càrrega de la bateria es considerarà baixa quan caigui a aquest nivell. "
"S'utilitzaran els paràmetres de bateria baixa en lloc de paràmetres de "
"bateria normals."

#: ui/GlobalConfig.qml:105
#, kde-format
msgctxt ""
"@label:spinbox Critical battery level percentage for the main power supply "
"battery"
msgid "Cr&itical level:"
msgstr "N&ivell crític:"

#: ui/GlobalConfig.qml:113
#, kde-format
msgctxt "@accessible:name:spinbox"
msgid "Critical battery level"
msgstr "Nivell crític de la bateria"

#: ui/GlobalConfig.qml:138
#, kde-format
msgctxt "@info:whatsthis"
msgid ""
"The battery charge will be considered critical when it drops to this level. "
"After a brief warning, the system will automatically suspend or shut down, "
"according to the configured critical battery level action."
msgstr ""
"La càrrega de la bateria es considerarà crítica quan caigui a aquest nivell. "
"Després d'un breu avís, el sistema automàticament se suspendrà o apagarà, "
"segons l'acció configurada de nivell de bateria crítica."

#: ui/GlobalConfig.qml:146
#, kde-format
msgctxt ""
"@label:combobox Power action such as sleep/hibernate that will be executed "
"when the critical battery level is reached"
msgid "A&t critical level:"
msgstr "En el nivell crí&tic:"

#: ui/GlobalConfig.qml:148
#, kde-format
msgctxt "@accessible:name:combobox"
msgid "Action performed at critical battery level"
msgstr "Acció que es porta a terme en el nivell crític de la bateria"

#: ui/GlobalConfig.qml:170
#, kde-format
msgctxt "@label:spinbox Low battery level percentage for peripheral devices"
msgid "Low level for peripheral d&evices:"
msgstr "Nivell baix per a dispositius p&erifèrics:"

#: ui/GlobalConfig.qml:178
#, kde-format
msgctxt "@accessible:name:spinbox"
msgid "Low battery level for peripheral devices"
msgstr "Nivell baix de bateria per a dispositius perifèrics"

#: ui/GlobalConfig.qml:203
#, kde-format
msgctxt "@info:whatsthis"
msgid ""
"The battery charge for peripheral devices will be considered low when it "
"reaches this level."
msgstr ""
"La càrrega de la bateria dels dispositius perifèrics es considerarà baixa "
"quan arribi a aquest nivell."

#: ui/GlobalConfig.qml:211
#, kde-format
msgctxt "@title:group"
msgid "Charge Limit"
msgstr "Límit de càrrega"

#: ui/GlobalConfig.qml:220
#, kde-format
msgctxt ""
"@label:spinbox Battery will stop charging when this charge percentage is "
"reached"
msgid "&Stop charging at:"
msgstr "A&tura la càrrega al:"

#: ui/GlobalConfig.qml:246
#, kde-format
msgctxt ""
"@label:spinbox Battery will start charging again when this charge percentage "
"is reached, after having hit the stop-charging threshold earlier"
msgid "Start charging once &below:"
msgstr "Inicia la càrrega per &sota de:"

#: ui/GlobalConfig.qml:303
#, kde-format
msgctxt "@info:status"
msgid ""
"You might have to disconnect and re-connect the power source to start "
"charging the battery again."
msgstr ""
"Potser cal desconnectar i tornar a connectar la font d'alimentació per a "
"tornar a iniciar la càrrega de la bateria."

#: ui/GlobalConfig.qml:313
#, kde-format
msgctxt "@info"
msgid ""
"Regularly charging the battery close to 100%, or fully discharging it, may "
"accelerate deterioration of battery health. By limiting the maximum battery "
"charge, you can help extend the battery lifespan."
msgstr ""
"La càrrega regular de la bateria a prop del 100%, o la descàrrega completa, "
"pot accelerar el deteriorament de la salut de la bateria. Limitant la "
"càrrega màxima de la bateria, podeu ajudar a allargar la vida útil de la "
"bateria."

#: ui/GlobalConfig.qml:321
#, kde-format
msgctxt ""
"@title:group Miscellaneous power management settings that didn't fit "
"elsewhere"
msgid "Other Settings"
msgstr "Altres paràmetres"

#: ui/GlobalConfig.qml:329
#, kde-format
msgctxt "@label:checkbox"
msgid "&Media playback:"
msgstr "Reproducció &multimèdia:"

#: ui/GlobalConfig.qml:330
#, kde-format
msgctxt "@text:checkbox"
msgid "Pause media players when suspending"
msgstr "Fa pausa dels reproductors multimèdia en suspendre"

#: ui/GlobalConfig.qml:343
#, kde-format
msgctxt "@label:button"
msgid "Related pages:"
msgstr "Pàgines relacionades:"

#: ui/GlobalConfig.qml:358
#, kde-format
msgctxt "@text:button Name of KCM, plus Power Management notification category"
msgid "Notifications: Power Management"
msgstr "Notificacions: Gestió d'energia"

#: ui/GlobalConfig.qml:360
#, kde-format
msgid "Open \"Notifications\" settings page, \"Power Management\" section"
msgstr ""
"Obre la pàgina de configuració de «Notificacions», secció «Gestió d'energia»"

#: ui/GlobalConfig.qml:367
#, kde-format
msgctxt "@text:button Name of KCM"
msgid "Desktop Session"
msgstr "Sessió d'escriptori"

#: ui/GlobalConfig.qml:368
#, kde-format
msgid "Open \"Desktop Session\" settings page"
msgstr "Obre la pàgina de configuració de «Sessió d'escriptori»"

#: ui/GlobalConfig.qml:375
#, kde-format
msgctxt "@text:button Name of KCM"
msgid "Activities"
msgstr "Activitats"

#: ui/GlobalConfig.qml:376
#, kde-format
msgid "Open \"Activities\" settings page"
msgstr "Obre la pàgina de configuració d'«Activitats»"

#: ui/main.qml:19
#, kde-format
msgctxt "@action:button"
msgid "Advanced Power &Settings…"
msgstr "Configuració avançada d'energia…"

#: ui/main.qml:43
#, kde-format
msgctxt "@text:placeholdermessage"
msgid "Power Management settings could not be loaded"
msgstr "No s'han pogut carregar els paràmetres de gestió d'energia"

#: ui/main.qml:79
#, kde-format
msgid "On AC Power"
msgstr "Amb alimentació CA"

#: ui/main.qml:85
#, kde-format
msgid "On Battery"
msgstr "Amb la bateria"

#: ui/main.qml:91
#, kde-format
msgid "On Low Battery"
msgstr "Amb la bateria baixa"

#: ui/ProfileConfig.qml:23
#, kde-format
msgctxt ""
"Percentage value example, used for formatting brightness levels in the power "
"management settings"
msgid "10%"
msgstr "10%"

#: ui/ProfileConfig.qml:36
#, kde-format
msgctxt "@title:group"
msgid "Suspend Session"
msgstr "Suspèn la sessió"

#: ui/ProfileConfig.qml:50
#, kde-format
msgctxt ""
"@label:combobox Suspend action such as sleep/hibernate to perform when the "
"system is idle"
msgid "A&fter a period of inactivity:"
msgstr "D&esprés d'un període d'inactivitat:"

#: ui/ProfileConfig.qml:58
#, kde-format
msgctxt "@accessible:name:combobox"
msgid "Action to perform when the system is idle"
msgstr "Acció a portar a terme quan el sistema no estigui ocupat"

#: ui/ProfileConfig.qml:103
#, kde-format
msgctxt ""
"@label:combobox Suspend action such as sleep/hibernate to perform when the "
"power button is pressed"
msgid "When &power button pressed:"
msgstr "Quan es &premi el botó d'encesa/apagat:"

#: ui/ProfileConfig.qml:105
#, kde-format
msgctxt "@accessible:name:combobox"
msgid "Action to perform when the power button is pressed"
msgstr "Acció a portar a terme quan es premi el botó d'encesa/apagat"

#: ui/ProfileConfig.qml:131
#, kde-format
msgctxt ""
"@label:combobox Suspend action such as sleep/hibernate to perform when the "
"power button is pressed"
msgid "When laptop &lid closed:"
msgstr "Quan es tanqui la &tapa del portàtil:"

#: ui/ProfileConfig.qml:133
#, kde-format
msgctxt "@accessible:name:combobox"
msgid "Action to perform when the laptop lid is closed"
msgstr "Acció a portar a terme quan es tanqui la tapa del portàtil"

#: ui/ProfileConfig.qml:159
#, kde-format
msgctxt ""
"@text:checkbox Trigger laptop lid action even when an external monitor is "
"connected"
msgid "Even when an external monitor is connected"
msgstr "Inclús quan hi hagi un monitor extern connectat"

#: ui/ProfileConfig.qml:161
#, kde-format
msgid "Perform laptop lid action even when an external monitor is connected"
msgstr ""
"Porta a terme l'acció de la tapa del portàtil Inclús quan hi hagi un monitor "
"extern connectat"

#: ui/ProfileConfig.qml:178
#, kde-format
msgctxt ""
"@label:combobox Sleep mode selection - suspend to memory, disk or both"
msgid "When sleeping, enter:"
msgstr "En adormir-se, passa a:"

#: ui/ProfileConfig.qml:180
#, kde-format
msgctxt "@accessible:name:combobox"
msgid "When sleeping, enter this power-save mode"
msgstr "En adormir-se, entra en aquest mode d'estalvi d'energia"

#: ui/ProfileConfig.qml:226
#, kde-format
msgctxt "@title:group"
msgid "Display and Brightness"
msgstr "Pantalla i lluminositat"

#: ui/ProfileConfig.qml:236
#, kde-format
msgctxt "@label:slider Brightness level"
msgid "Change scr&een brightness:"
msgstr "Canvia la lluminositat de la p&antalla:"

#: ui/ProfileConfig.qml:279
#, kde-format
msgctxt "@label:spinbox Dim screen after X minutes"
msgid "Di&m automatically:"
msgstr "Enfosqueix auto&màticament:"

#: ui/ProfileConfig.qml:319
#, kde-format
msgctxt "@label:spinbox After X minutes"
msgid "&Turn off screen:"
msgstr "Apaga la pan&talla:"

#: ui/ProfileConfig.qml:358
#, kde-format
msgctxt "@label:spinbox After X seconds"
msgid "When loc&ked, turn off screen:"
msgstr "Quan està blo&quejada, apaga la pantalla:"

#: ui/ProfileConfig.qml:391
#, kde-format
msgctxt "@label:slider Brightness level"
msgid "Change key&board brightness:"
msgstr "Canvia la lluminositat del te&clat:"

#: ui/ProfileConfig.qml:438
#, kde-format
msgctxt "@title:group"
msgid "Other Settings"
msgstr "Altres paràmetres"

#: ui/ProfileConfig.qml:446
#, kde-format
msgctxt ""
"@label:combobox Power Save, Balanced or Performance profile - same options "
"as in the Battery applet"
msgid "Switch to po&wer profile:"
msgstr "Canvia al perfil d'e&nergia:"

#: ui/ProfileConfig.qml:450
#, kde-format
msgctxt ""
"@accessible:name:combobox Power Save, Balanced or Performance profile - same "
"options as in the Battery applet"
msgid "Switch to power profile"
msgstr "Canvia al perfil d'energia"

#: ui/ProfileConfig.qml:480
#, kde-format
msgctxt "@label:button"
msgid "Run custom script:"
msgstr "Executa un script personalitzat:"

#: ui/ProfileConfig.qml:486
#, kde-format
msgctxt ""
"@text:button Determine what will trigger a script command to run in this "
"power state"
msgid "Choose run conditions…"
msgstr "Trieu les condicions d'execució…"

#: ui/ProfileConfig.qml:488
#, kde-format
msgctxt "@accessible:name:button"
msgid "Choose run conditions for script command"
msgstr "Trieu les condicions d'execució de l'ordre de l'script"

#: ui/ProfileConfig.qml:507
#, kde-format
msgctxt "@text:action:menu Script command to execute"
msgid "When e&ntering this power state"
msgstr "En e&ntrar en aquest estat d'energia"

#: ui/ProfileConfig.qml:523
#, kde-format
msgctxt "@text:action:menu Script command to execute"
msgid "When e&xiting this power state"
msgstr "En &sortir d'aquest estat d'energia"

#: ui/ProfileConfig.qml:539
#, kde-format
msgctxt "@text:action:menu Script command to execute"
msgid "After a period of inacti&vity"
msgstr "Després d'un període d'inacti&vitat"

#: ui/ProfileConfig.qml:558
#, kde-format
msgctxt "@label:textfield Script command to execute"
msgid "When e&ntering this power state:"
msgstr "En e&ntrar en aquest estat d'energia:"

#: ui/ProfileConfig.qml:560
#, kde-format
msgctxt "@label:textfield"
msgid "Script command when entering this power state"
msgstr "Ordre de l'script en entrar en aquest estat d'energia"

#: ui/ProfileConfig.qml:591
#, kde-format
msgctxt "@label:textfield Script command to execute"
msgid "When e&xiting this power state:"
msgstr "En &sortir d'aquest estat d'energia:"

#: ui/ProfileConfig.qml:593
#, kde-format
msgctxt "@label:textfield"
msgid "Script command when exiting this power state"
msgstr "Ordre de l'script en sortir d'aquest estat d'energia"

#: ui/ProfileConfig.qml:624
#, kde-format
msgctxt "@label:textfield Script command to execute"
msgid "After a period of inacti&vity:"
msgstr "Després d'un període d'inacti&vitat:"

#: ui/ProfileConfig.qml:626
#, kde-format
msgctxt "@@accessible:name:textfield"
msgid "Script command after a period of inactivity"
msgstr "Ordre de l'script després d'un període d'inacti&vitat"

#: ui/ProfileConfig.qml:655
#, kde-format
msgctxt "@accessible:name:spinbox"
msgid "Period of inactivity until the script command executes"
msgstr "Període d'inactivitat fins que s'executi l'ordre de l'script"

#: ui/RunScriptEdit.qml:26
#, kde-format
msgid "Enter command or select file…"
msgstr "Introduïu una ordre o seleccioneu un fitxer…"

#: ui/RunScriptEdit.qml:43
#, kde-format
msgid "Select executable file…"
msgstr "Seleccioneu un fitxer executable…"

#: ui/RunScriptEdit.qml:58
#, kde-format
msgid "Select executable file"
msgstr "Seleccioneu un fitxer executable"

#: ui/TimeDelaySpinBox.qml:14
#, kde-format
msgctxt ""
"List of recognized strings for 'minutes' in a time delay expression such as "
"'after 10 min'"
msgid "m|min|mins|minutes"
msgstr "m|min|mins|minuts"

#: ui/TimeDelaySpinBox.qml:15
#, kde-format
msgctxt ""
"List of recognized strings for 'seconds' in a time delay expression such as "
"'after 10 sec'"
msgid "s|sec|secs|seconds"
msgstr "s|seg|segs|segons"

#: ui/TimeDelaySpinBox.qml:17
#, kde-format
msgctxt ""
"Validator/extractor regular expression for a time delay number and unit, "
"from e.g. 'after 10 min'. Uses recognized strings for minutes and seconds as "
"%1 and %2."
msgid "[^\\d]*(\\d+)\\s*(%1|%2)\\s*"
msgstr "[^\\d]*(\\d+)\\s*(%1|%2)\\s*"

#: ui/TimeDelaySpinBox.qml:26
#, kde-format
msgid "after %1 min"
msgid_plural "after %1 min"
msgstr[0] "després d'1 min"
msgstr[1] "després de %1 min"

#: ui/TimeDelaySpinBox.qml:28
#, kde-format
msgid "after %1 sec"
msgid_plural "after %1 sec"
msgstr[0] "després d'1 s"
msgstr[1] "després de %1 s"
